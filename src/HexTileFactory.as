package
{
	import flash.events.MouseEvent;
	import HexGrid.*;
	/**
	 * ...
	 * @author Michael
	 */
	public class HexTileFactory implements IHexTileFactory
	{
		private var scale:Number = 1.0;
		
		public function HexTileFactory(tileScale:Number)
		{
			//trace("HexTileFactory constructor");
			scale = tileScale;
		}
		
		public function CreateHexTile(coordinate:CubeCoordinate):IHexTile
		{
			//trace("CreateHexTile()");
			
			var graphic:TileSprite = new TileSprite();
			graphic.scaleX = scale;
			graphic.scaleY = scale;
			var color:uint = TileColors.RandomColor();
			
			var newTile:HexTile = new HexTile(graphic, coordinate, color);
			newTile.ConnectEventListeners();
			
			return newTile;
		}
	}

}