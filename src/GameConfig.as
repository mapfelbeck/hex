package  
{
	/**
	 * ...
	 * @author ...
	 */
	public class GameConfig  implements IConfig
	{
		private var x:int;
		private var y:int;
		
		private var size:int;
		
		public function GameConfig(sizeX:int, sizeY:int, boardSize:int) 
		{
			x = sizeX;
			y = sizeY;
			
			size = boardSize;
		}
		
		public function get BoardSize():int
		{
			return size;
		}
		
		public function get ScreenWidth():int
		{
			return x;
		}
		
		public function get ScreenHeight():int
		{
			return y;
		}
	}

}