package
{
	import flash.display.*;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.Dictionary;
	import HexGrid.*;
	import HexGrid.Events.*;
	import com.greensock.*;
	import flash.system.Capabilities;
	/**
	 * ...
	 * @author Michael
	 */
	public class Main extends MovieClip
	{
		private var stageCenterX:int;
		private var stageCenterY:int;
		
		private var grid:HexGrid;
		private var gridUpdated:Boolean;
		
		//tiles fall at 4 tile/s speed
		private var fallRateInTiles:Number = 4.0;
		private var fallRateInPx:Number = 0.0;
		
		private var factory:HexTileFactory;
		
		private var animCount:int = 0;
		private var wasAnimating:Boolean = false;
		
		private var coordinateSpaceRingCount:int = 3;
		
		private var config:IConfig;
		
		private var highlightGlow:GlowFilter = null;
		private var selectedGlow:GlowFilter = null;
		
		public function Main():void
		{
			if((Capabilities.os.indexOf("Windows") >= 0))
			{
				config = new WebConfig(stage);
			}
			else if((Capabilities.os.indexOf("Mac") >= 0))
			{
				config = new WebConfig(stage);
			}
			else if((Capabilities.os.indexOf("Linux") >= 0))
			{
				config = new WebConfig(stage);
			}
			else
			{
				config = new AirConfig();
			}
			stage.scaleMode = StageScaleMode.NO_SCALE;
			//stage.align = StageAlign.TOP_LEFT;
			//stage.addEventListener(Event.DEACTIVATE, deactivate);
			
			stageCenterX = config.ScreenWidth / 2;
			stageCenterY = config.ScreenHeight / 2;
			
			//trace("player OS: " + Capabilities.os);
			//trace("screen size: "+config.ScreenWidth+"x"+config.ScreenHeight);
			//trace("stage center: "+stageCenterX+"x"+stageCenterY);
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			var coordianteSpaceWidth:int = coordinateSpaceRingCount * 2 + 1;
			var tileScaleX:Number = 0.0;
			var tileScaleY:Number = 0.0;
			var testTile:TileSprite = new TileSprite();
			//tiles overlap by 25%
			var tileWidth = testTile.width;
			var tileHeight = testTile.height;
			
			//tiles overlap by 25%, width is *.75% +25% of one tile
			var unscaledWidth:int = (tileWidth * coordinateSpaceRingCount * 2 * .75) + tileWidth;
			var unscaledHeight:int = testTile.width * coordianteSpaceWidth;
			
			//how much of the screen to take up
			var scale:Number = .95;
			
			var screenBoundsWidth:int = config.ScreenWidth * scale;
			var screenBoundsHeight:int = config.ScreenHeight * scale;
			
			//trace("un-scaled width: " + unscaledWidth);
			//trace("un-scaled height: " + unscaledHeight);
			
			//trace("screenBoundsWidth: " + screenBoundsWidth);
			//trace("screenBoundsHeight: " + screenBoundsHeight);
			
			tileScaleX = screenBoundsWidth / unscaledWidth;
			tileScaleY = screenBoundsHeight / unscaledHeight;
			
			highlightGlow = new GlowFilter();
			highlightGlow.color = 0xCCCCCC;
			highlightGlow.inner = true;
			highlightGlow.blurX = 15;
			highlightGlow.blurY = 15;
			
			selectedGlow = new GlowFilter();
			selectedGlow.color = 0xCCCCCC;
			selectedGlow.inner = true;
			selectedGlow.blurX = 25;
			selectedGlow.blurY = 25;
			
			factory = new HexTileFactory(Math.min(tileScaleX, tileScaleY));
			
			gridUpdated = false;
			grid = new HexGrid(factory);
			
			var coordSpace:Vector.<CubeCoordinate> = grid.GenerateCoordinateSpace(coordinateSpaceRingCount);
			grid.GenerateTilesFromCoordSpace(coordSpace);
			
			AttachGridToStage(grid);
			
			//get a tile for reference and use it to figure out the tile fall rate in px/s
			var refTile:IHexTile = grid.GridLookup(new AxialCoordinate(0, 0));
			var refTileHeight:Number = refTile.HexSprite.height;
			fallRateInPx = fallRateInTiles * refTileHeight;
			
			grid.addEventListener(HexEvents.GRID_CONTENTS_CHANGED, GridChanged);
			
			addEventListener(Event.ENTER_FRAME, Update, false, 0, true);
			gridUpdated = true;
		}
		
		private function GridChanged(event:GridContentsChangedEvent):void
		{
			//trace("GridChanged, need to fix up sprites");
			gridUpdated = true;
		}
		
		private function AnimFinish():void
		{
			--animCount;
		}
		
		public function Update(e:Event):void
		{
			//trace("Main: Update()");
			if (gridUpdated)
			{
				//trace("grid has been updated");
				gridUpdated = false;
				var tile:HexTile = null;
				while (grid.MovedTiles.length > 0)
				{
					tile = grid.MovedTiles.shift();
					SetTileSpriteLocation(tile, true);
				}
				
				while (grid.DeletedTiles.length > 0)
				{
					tile = grid.DeletedTiles.shift();
					tile.removeEventListener(HexEvents.TILE_OVER, TileOver);
					tile.removeEventListener(HexEvents.TILE_OUT, TileOut);
					tile.removeEventListener(HexEvents.TILE_BEGIN, TileBegin);
					tile.removeEventListener(HexEvents.TILE_END, TileEnd);
					tile.HexSprite.parent.removeChild(tile.HexSprite);
				}
				
				while (grid.ReplacementTiles.length > 0)
				{
					var tileContainer:ReplacementHex = grid.ReplacementTiles.shift();
					var dir:uint = tileContainer.dir;
					tile = tileContainer.hex as HexTile;
					grid.AddTileToGrid(tile);
					addChild(tile.HexSprite);
					
					//tile tweens in from outside the board, the board is a symetrical hex
					//so just getting the distance from center > than grid bound works
					var zeroCoord:AxialCoordinate = new AxialCoordinate(0, 0);
					var startCoord:AxialCoordinate = tile.axialCoordinate;
					while (AxialCoordinate.Distance(zeroCoord, startCoord) <= grid.Size)
					{
						startCoord = startCoord.GetCoordInDirection(dir);
					}
					//copy-pasta from SetTileSpriteLocation, collapse 2 code chunkies into one later
					var sqrt3:Number = Math.sqrt(3);
					var halfHeight:Number;
					var halfWidth:Number;
					var relativeX:Number;
					var relativeY:Number;
					halfHeight = tile.HexSprite.height / 2;
					halfWidth = tile.HexSprite.width / 2;
					relativeX = halfWidth * 3 / 2 * startCoord.q;
					relativeY = halfWidth * sqrt3 * (startCoord.r + startCoord.q / 2);
					tile.HexSprite.x = stageCenterX - halfWidth + relativeX;
					tile.HexSprite.y = stageCenterY - halfHeight + relativeY;
					
					SetTileSpriteLocation(tile, true);
					tile.addEventListener(HexEvents.TILE_OVER, TileOver);
					tile.addEventListener(HexEvents.TILE_OUT, TileOut);
					tile.addEventListener(HexEvents.TILE_BEGIN, TileBegin);
					tile.addEventListener(HexEvents.TILE_END, TileEnd);
				}
			}
			
			if (animCount == 0 && wasAnimating)
			{
				wasAnimating = false;
				//trace("done animating");
				
				var matches:Dictionary;
				matches = CheckForLines();
				DeleteMatches(matches);
				matches = null;
			}
			else if (animCount > 0 )
			{
				wasAnimating = true;
				//trace("still animating");
			}
		}
		
		private var lastHighlighted:HexTile;
		private var lastSelected:HexTile;
		
		public function TileOver(event:TileClickEvent):void
		{
			//trace("Main: TileOver");
			if (wasAnimating)
			{
				return;
			}
			
			var tile:HexTile = event.target as HexTile;
			
			if (null == lastHighlighted)
			{
				lastHighlighted = tile;
			}
			else
			{
				lastHighlighted = tile;
			}
			
			if (lastHighlighted != lastSelected)
			{
				lastHighlighted.HexSprite.filters = [highlightGlow];
			}
		}
		
		private function TileOut(event:TileClickEvent):void
		{
			//trace("Main: TileOut");
			if (wasAnimating)
			{
				return;
			}
			
			var tile:HexTile = event.target as HexTile;
			
			if (null != lastHighlighted && tile == lastSelected)
			{
				lastHighlighted.HexSprite.filters = [selectedGlow];
			}
			else if(null != lastHighlighted)
			{
				lastHighlighted.HexSprite.filters = [];
			}
		}
		
		private function TileBegin(event:TileClickEvent):void
		{
			//trace("Main: TileBegin");
			if (wasAnimating)
			{
				return;
			}
			
			var tile:HexTile = event.target as HexTile;
			
			if (null == lastSelected)
			{
				lastSelected = tile;
				lastSelected.HexSprite.filters = [selectedGlow];
			}
			else
			{
				CheckDirectionAndSwap(lastSelected, tile, 1);
				
				lastSelected.HexSprite.filters = [];
				tile.HexSprite.filters = [];
				if (null != lastHighlighted)
				{
					lastHighlighted.HexSprite.filters = [];
					lastHighlighted = null;
				}
				lastSelected = null;
			}
		}
		
		private function TileEnd(event:TileClickEvent):void
		{
			//trace("Main: TileEnd");
			if (wasAnimating)
			{
				return;
			}
			
			var tile:HexTile = event.target as HexTile;
			if (tile == lastSelected)
			{
				return;
			}
			else if(null != lastSelected)
			{
				CheckDirectionAndSwap(lastSelected, tile, 2);
				
				lastSelected.HexSprite.filters = [];
				tile.HexSprite.filters = [];
				if (null != lastHighlighted)
				{
					lastHighlighted.HexSprite.filters = [];
					lastHighlighted = null;
				}
			}
			lastSelected = null;
		}
		
		private function CheckDirectionAndSwap(tile1:IHexTile, tile2:IHexTile, maxDistance:int):Boolean
		{
			//trace("CheckDirectionAndSwap");
			var swapped:Boolean = false;
			
			if (null == tile1 || null == tile2)
			{
				trace("Error: CheckDirectionAndSwap given null for tile1 or tile2");
				return swapped;
			}
			
			if(maxDistance >= AxialCoordinate.Distance(tile1.axialCoordinate, tile2.axialCoordinate))
			{
				var direction:Number = TileDirection(tile1, tile2);
				if (-1 != direction)
				{
					swapped = true;
					//trace("clicked tile in swappable direction");
					var adjacentTile:IHexTile = grid.GridLookup(lastSelected.axialCoordinate.GetCoordInDirection(direction));
					grid.SwapTiles(tile1, adjacentTile);
					wasAnimating = true;
				}
			}
			
			return swapped;
		}
		
		private function TileDirection(tile1:IHexTile, tile2:IHexTile):int
		{
			var direction:int = -1;
			
			var coord1:CubeCoordinate = tile1.cubeCoordinate;
			var coord2:CubeCoordinate = tile2.cubeCoordinate;
			
			//if exactly 1 coordinate between these 2 tiles is the same
			if ((coord1.x == coord2.x && coord1.y != coord2.y && coord1.z != coord2.z) ||
				(coord1.x != coord2.x && coord1.y == coord2.y && coord1.z != coord2.z) ||
				(coord1.x != coord2.x && coord1.y != coord2.y && coord1.z == coord2.z))
			{
				Math.min(x, 1)
				Math.max(x, -1)
				var dX:int = Clamp(coord2.x - coord1.x, -1, 1);
				var dY:int = Clamp(coord2.y - coord1.y, -1, 1);
				var dZ:int = Clamp(coord2.z - coord1.z, -1, 1);
				
				direction = CubeCoordinate.AdjacentCoordToDirection(new CubeCoordinate(dX, dY, dZ));
			}
			
			return direction;
		}
		private function Clamp(x:Number, min:Number, max:Number)
		{
			return Math.min(Math.max(x, min), max);
		}
		
		private function DeleteMatches(matches:Dictionary)
		{
			//trace("DeleteMatches");
			for each(var match:Vector.<Vector.<HexTile>> in matches)
			{
				//trace("I see a match " + match);
				for each(var list:Vector.<HexTile> in match)
				{
					//trace("...");
					for each(var tile:HexTile in list)
					{
						grid.Delete(tile);
					}
				}
			}
		}
		
		private function matchKey(dir:uint, color:uint):int
		{
			var key:int = 0;
			
			switch(dir)
			{
				case HexDirection.UP:
				case HexDirection.DOWN:
					key += 1;
					break;
				case HexDirection.UPPER_RIGHT:
				case HexDirection.LOWER_LEFT:
					key += 2;
					break;
				case HexDirection.LOWER_RIGHT:
				case HexDirection.UPPER_LEFT:
					key += 3;
					break;
				default:
					throw Error("matchKey given bad direction");
					break;
			}
			switch(color)
			{
				case TileColors.BLUE:
					key += 10;
					break;
				case TileColors.CYAN:
					key += 20;
					break;
				case TileColors.GREEN:
					key += 30;
					break;
				case TileColors.PURPLE:
					key += 40;
					break;
				case TileColors.RED:
					key += 50;
					break;
				case TileColors.YELLOW:
					key += 60;
					break;
				case TileColors.WHITE:
					key += 70;
					break;
				default:
					throw Error("matchKey given bad color");
					break;
			}
			
			return key;
		}
		
		private var matchChains:Dictionary;
		private static const checkDirections:Array = [HexDirection.UP, HexDirection.UPPER_RIGHT, HexDirection.LOWER_RIGHT];
		private function CheckForLines():Dictionary
		{
			//trace("CheckForLines()");
			
			matchChains = new Dictionary(true);
			
			var tileChain:Vector.<HexTile>;
			
			for each (var tile:HexTile in grid.Tiles)
			{
				
				var tileCoord:AxialCoordinate = tile.axialCoordinate;
				//trace("checking tile with coord: " + tileCoord.toString());
				for each(var dir :uint in checkDirections)
				{
					tileChain = new Vector.<HexTile>();
					tileChain.push(tile);
					//trace("in direction: " + HexDirection.DirectionName(dir));
					var lookupCoord:AxialCoordinate = tileCoord.GetCoordInDirection(dir);
					//var lineLength:int = 1;
					//trace("check dir: " + lookupCoord.toString());
					var lookupTile:HexTile;
					lookupTile = grid.GridLookup(lookupCoord) as HexTile;
					
					//while we have tiles to look at
					while (null != lookupTile)
					{
						//trace("checking against tile with coord: " + lookupTile.axialCoordinate.toString());
						if (tile.Color == lookupTile.Color)
						{
							//trace("color match");
							tileChain.push(lookupTile);
							//lineLength++;
							lookupCoord = lookupCoord.GetCoordInDirection(dir);
							lookupTile = grid.GridLookup(lookupCoord) as HexTile;
							//trace(tileCoord.toString() + "matches a tile at " + lookupCoord.toString());
						}
						else
						{
							lookupTile = null;
						}
					}
					
					if (tileChain.length > 2)
					{
						//trace(tileCoord.toString() + " is part of a " + TileColors.ColorName(tile.Color) + " length " + tileChain.length + " line in the " + HexDirection.DirectionName(dir) + " direction");
						var key = matchKey(dir, tile.Color);
						//no existing chains in this direction
						if (null == matchChains[key])
						{
							var newMatches:Vector.<Vector.<HexTile>> = new Vector.<Vector.<HexTile>>();
							newMatches.push(tileChain);
							//trace("adding list");
							matchChains[key] = newMatches;
						}
						//existing chains in this direction
						else
						{
							var existingMatches:Vector.<Vector.<HexTile>> = matchChains[key];
							//trace(existingList.length + " lists already exist");
							var foundInExisting:Boolean = false;
							for each(var list:Vector.<HexTile> in existingMatches)
							{
								//trace("wat?");
								var existingIndex = list.indexOf(tile);
								if (existingIndex != -1)
								{
									foundInExisting = true;
									//trace(tileChain.length + " vs. " + list.length);
									if (tileChain.length > list.length)
									{
										//trace("the old list needs to be removed...");
										foundInExisting = false;
										existingMatches.splice(existingIndex, 1);
									}
								}
							}
							if (!foundInExisting)
							{
								//trace("not found in existing list");
								existingMatches.push(tileChain);
							}
						}
					}
					
					tileChain = null;
				}
			}
			
			return matchChains;
		}
		
		private function AttachGridToStage(grid:HexGrid):void
		{
			//trace("AttachGridToStage");

			for each(var tile:HexTile in grid.Tiles)
			{
				SetTileSpriteLocation(tile);
				addChild(tile.HexSprite);
				tile.addEventListener(HexEvents.TILE_OVER, TileOver);
				tile.addEventListener(HexEvents.TILE_OUT, TileOut);
				tile.addEventListener(HexEvents.TILE_BEGIN, TileBegin);
				tile.addEventListener(HexEvents.TILE_END, TileEnd);
			}
		}
		
		private function SetTileSpriteLocation(tile:HexTile, animate:Boolean = false):void
		{
			//trace("SetTileSpriteLocation");
			var sqrt3:Number = Math.sqrt(3);
			
			//half the sprite's dimensions
			var halfHeight:Number;
			var halfWidth:Number;
			//sprite's relative offset in the hex grid
			var relativeX:Number;
			var relativeY:Number;
			var coord:AxialCoordinate = tile.axialCoordinate;
			halfHeight = tile.HexSprite.height / 2;
			halfWidth = tile.HexSprite.width / 2;
			
			relativeX = halfWidth * 3 / 2 * coord.q;
			relativeY = halfWidth * sqrt3 * (coord.r + coord.q / 2);
			
			if (animate)
			{
				animCount++;
				var deltaX:Number = tile.HexSprite.x - (stageCenterX - halfWidth + relativeX);
				var deltaY:Number = tile.HexSprite.y - (stageCenterY - halfHeight + relativeY);
				var totalDelta:Number = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
				var tweenTime:Number = totalDelta / fallRateInPx;
				
				TweenLite.to(tile.HexSprite, tweenTime, { x:stageCenterX - halfWidth + relativeX, y:stageCenterY - halfHeight + relativeY, onComplete :AnimFinish } );
			}
			else
			{
				tile.HexSprite.x = stageCenterX - halfWidth + relativeX;
				tile.HexSprite.y = stageCenterY - halfHeight + relativeY;
			}
		}
		
		private function deactivate(e:Event):void
		{
			// auto-close
			//NativeApplication.nativeApplication.exit();
		}
		
	}
	
}