﻿package Screens
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	/**
	 * ...
	 * @author ...
	 */
	public class MenuScreen extends BaseScreen
	{
		public var button1:BasicButton;
		public var button2:BasicButton;
		public var button3:BasicButton;
		
		public function MenuScreen()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(event:Event)
		{
			trace("Init");
			
			this.removeEventListener(Event.ADDED_TO_STAGE, Init);
			
			//button1.addEventListener(MouseEvent.MOUSE_UP, Game1);
			
			
			if (Multitouch.supportsTouchEvents)
			{
				Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
				//trace("Touch events supported");
				button1.addEventListener(TouchEvent.TOUCH_BEGIN, Game1);
				button2.addEventListener(TouchEvent.TOUCH_BEGIN, Game2);
				button3.addEventListener(TouchEvent.TOUCH_BEGIN, Game3);
				//button1.addEventListener(MouseEvent.CLICK, Game1);
			}
			else
			{
				button1.addEventListener(MouseEvent.CLICK, Game1);
				button2.addEventListener(MouseEvent.CLICK, Game2);
				button3.addEventListener(MouseEvent.CLICK, Game3);
				//button1.addEventListener(TouchEvent.TOUCH_BEGIN, Game1);
			}
			//button1.background.addEventListener(MouseEvent.MOUSE_UP, Game2);
			button1.button_text.text = "Regular";
			button2.button_text.text = "Large";
			button3.button_text.text = "Ridiculous";
			
		}
		
		private function Game1(event:Event)
		{
			trace("Game1");
			var gameScreen:BaseScreen = new GameScreen(3);
			this.stage.addChild(gameScreen);
		}
		private function Game2(event:Event)
		{
			trace("Game2");
			var gameScreen:BaseScreen = new GameScreen(4);
			this.stage.addChild(gameScreen);
		}
		private function Game3(event:Event)
		{
			trace("Game3");
			var gameScreen:BaseScreen = new GameScreen(5);
			this.stage.addChild(gameScreen);
		}
		private function TurnOnGame(event:Event)
		{
			//trace("TurnOnGame");
			
			//event.currentTarget.removeEventListener(Event.ADDED_TO_STAGE, TurnOnGame);
		}
	}

}
