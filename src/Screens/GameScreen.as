package Screens 
{
	import flash.display.MovieClip;
	import flash.system.Capabilities;
	import flash.events.*;
	import HexGrid.Events.*;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author ...
	 */
	public class GameScreen extends BaseScreen 
	{
		private var config:IConfig;
		private var board:GameBoard;
		
		private var pointsLookup:Dictionary;
		private var gameScore:int = 0;
		
		private var boardSize:int = 3;
		
		public function GameScreen(size:int) 
		{
			trace("oh hai I'm GameScreen");
			trace(this.anchor);
			
			if (size > 0)
			{
				boardSize = size;
			}
			
			pointsLookup = new Dictionary();
			pointsLookup[3] = 100;
			pointsLookup[4] = 250;
			pointsLookup[5] = 500;
			pointsLookup[6] = 1000;
			pointsLookup[7] = 5000;
			
			this.addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(event:Event)
		{
			trace("Init");
			this.removeEventListener(Event.ADDED_TO_STAGE, Init);
			if((Capabilities.os.indexOf("Windows") >= 0))
			{
				config = new WebConfig(this.stage);
			}
			else if((Capabilities.os.indexOf("Mac") >= 0))
			{
				config = new WebConfig(this.stage);
			}
			else if((Capabilities.os.indexOf("Linux") >= 0))
			{
				config = new WebConfig(this.stage);
			}
			else
			{
				config = new AirConfig();
			}
			
			LoadGameBoard();
		}
		
		private function LoadGameBoard():void
		{
			trace("LoadGameBoard");
			
			var boardConfig = new GameConfig(config.ScreenWidth - this.anchor.x, config.ScreenHeight - this.anchor.y, boardSize);
			trace("anchor location: (" + this.anchor.x + ", " + this.anchor.y + ")");
			trace("(" + boardConfig.ScreenWidth + ", " + boardConfig.ScreenHeight + ")");
			
			board = new GameBoard(boardConfig);
			board.addEventListener(HexEvents.GRID_MATCH, OnMatch);
			board.addEventListener(Event.ADDED_TO_STAGE, InitBoard);
			anchor.addChild(board);
		}
		
		private function InitBoard(event:Event):void
		{
			trace("InitBoard");
			board.removeEventListener(Event.ADDED_TO_STAGE, InitBoard);
			board.Init();
		}
		
		private var cascadeRatio:Number = 1.5;
		private function OnMatch(event:GridMatchEvent):void
		{			
			try
			{
				var points:int = pointsLookup[event.MatchSize];
				var cascadeBonus:Number = Math.pow(cascadeRatio, event.Cascade-1);
				var finalPoints = points * cascadeBonus;
				
				//trace("Match size: " + event.MatchSize + ", base points: " + points + ", cascade count: " + event.Cascade + ", cascade bonus multiplier: " + cascadeBonus + ", final points: " + finalPoints);
				gameScore += finalPoints;
				this.score.score_text.text = gameScore.toString();
			}
			catch (error:Error)
			{
				trace("something went wrong looking up points.");
			}
		}
	}

}