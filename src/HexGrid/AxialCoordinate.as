package HexGrid
{
	/**
	 * ...
	 * @author Michael
	 */
	public class AxialCoordinate
	{
		private static var axialNeighbors:Array = [
								[ 0, -1], [1, -1], [1,  0],
								[ 0, 1], [ -1, 1], [ -1,  0]];
								
		private static var axialDiagonals:Array = [
								[1, 1], [2, -1], [1, -2],
								[ -1, -1], [ -2, 1], [ -1, 2]];
								
		private var _q:Number = 0;
		public function get q():Number { return _q; }
		
		private var _r:Number = 0;
		public function get r():Number { return _r; }
		
		public function AxialCoordinate(coordQ:Number = 0, coordR:Number = 0)
		{
			_q = coordQ;
			_r = coordR;
		}
		
		public function AsCube():CubeCoordinate
		{
			return new CubeCoordinate(q, -q - r, r);
		}
		
		public function GetCoordInDirection(dir:uint):AxialCoordinate
		{
			var d:Array = axialNeighbors[dir];
			return new AxialCoordinate(_q + d[0], _r + d[1]);
		}
		
		public static function AdjacentCoordToDirection(coord:AxialCoordinate):uint
		{
			var dir:uint = 0;
			var i:uint = 0;
			for (i = 0; i <= axialNeighbors.length; i++)
			{
				var d:Array = axialNeighbors[i];
				if (d[0] == coord.q && d[1] == coord.r)
				{
					dir = i;
					
					break;
				}
			}
			
			return dir;
		}
		
		public static function Distance(coord1:AxialCoordinate,coord2:AxialCoordinate):Number
		{
			return (Math.abs(coord1.q - coord2.q) + Math.abs(coord1.r - coord2.r) +
					Math.abs(coord1.q + coord1.r - coord2.q - coord2.r)) / 2;
		}
		
		public function toString():String
		{
			return "[" + _q + "," + _r + "]";
		}
	}

}