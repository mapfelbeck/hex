package HexGrid
{
	import flash.display.MovieClip;
	
	/**
	 * ...
	 * @author Michael
	 */
	public interface IHexTile
	{
		function get HexSprite():MovieClip;
		function set HexSprite(newSprite:MovieClip):void;
		
		function get cubeCoordinate():CubeCoordinate;
		function set cubeCoordinate(newCoord:CubeCoordinate):void;
		
		function get axialCoordinate():AxialCoordinate;
		function set axialCoordinate(newCoord:AxialCoordinate):void;
		
		function Destroy():void;
	}
	
}