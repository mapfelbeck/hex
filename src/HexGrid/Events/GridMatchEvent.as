package HexGrid.Events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Michael
	 */
	public class GridMatchEvent extends Event 
	{
		public var MatchSize:int = 0;
		public var Cascade:int = 0;
		
		public function GridMatchEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new GridMatchEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("GridMatchEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}