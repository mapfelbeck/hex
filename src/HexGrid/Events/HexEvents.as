package HexGrid.Events
{
	/**
	 * ...
	 * @author Michael
	 */
	public class HexEvents
	{
		public static var GRID_CONTENTS_CHANGED:String = "GridContentsChanged";
		public static var TILE_CLICKED:String = "TileClicked";
		public static var TILE_OVER:String = "TileOver";
		public static var TILE_OUT:String = "TileOut";
		public static var TILE_BEGIN:String = "TileBegin";
		public static var TILE_END:String = "TileEnd";
		public static var GRID_MATCH:String = "GridMatch";
	}

}