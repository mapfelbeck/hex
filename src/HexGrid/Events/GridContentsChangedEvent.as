package HexGrid.Events
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Michael
	 */
	public class GridContentsChangedEvent extends Event
	{
		public function GridContentsChangedEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
		}
		
		public override function clone():Event
		{
			return new GridContentsChangedEvent(type, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return formatToString("GridContentsChangedEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
		
	}
	
}