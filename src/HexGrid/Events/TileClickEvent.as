package HexGrid.Events
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Michael
	 */
	public class TileClickEvent extends Event
	{
		public function TileClickEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
		}
		
		public override function clone():Event
		{
			return new TileClickEvent(type, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return formatToString("TileClick", "type", "bubbles", "cancelable", "eventPhase");
		}
	}
	
}