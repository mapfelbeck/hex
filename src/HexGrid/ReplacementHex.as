package HexGrid
{
	/**
	 * ...
	 * @author Michael
	 */
	public class ReplacementHex
	{
		public var hex:IHexTile;
		public var dir:uint;
		public function ReplacementHex(newHex:IHexTile, newDir:uint)
		{
			hex = newHex;
			dir = newDir;
		}
		
	}

}