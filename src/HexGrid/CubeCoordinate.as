package HexGrid
{
	/**
	 * ...
	 * @author Michael
	 */
	public class CubeCoordinate
	{
		private static var cubeNeighbors:Array = [
								[ 0, 1, -1], [1,  0, -1], [1, -1,  0],
								[ 0, -1, 1], [ -1,  0, 1], [ -1, 1,  0]];
								
		private static var cubeDiagonals:Array = [
								[2, -1, -1], [1, 1, -2], [-1, 2, -1],
								[ -2, 1, 1], [ -1, -1, 2], [ 1, -2, 1]];
								
		private var _x:Number = 0;
		public function get x():Number { return _x; }
		
		private var _y:Number = 0;
		public function get y():Number { return _y; }
		
		private var _z:Number = 0;
		public function get z():Number { return _z;}
		
		public function get isValid():Boolean { return x + y + z == 0;}
		
		public function CubeCoordinate(coordX:Number = 0, coordY:Number = 0, coordZ:Number = 0)
		{
			_x = coordX;
			_y = coordY;
			_z = coordZ;
			//trace(_x + "+" + _y + "+" + _z + "="+(_x + _y + _z));
		}
		
		public function AsAxial():AxialCoordinate
		{
			return new AxialCoordinate(_x, _z);
		}
		
		public function GetCoordInDirection(dir:uint):CubeCoordinate
		{
			var d:Array = cubeNeighbors[dir];
			return new CubeCoordinate(_x + d[0], _y + d[1], _z + d[2]);
		}
		
		public static function AdjacentCoordToDirection(coord:CubeCoordinate):uint
		{
			var dir:uint = 0;
			var i:uint = 0;
			for (i = 0; i <= cubeNeighbors.length; i++)
			{
				var d:Array = cubeNeighbors[i];
				if (d[0] == coord.x && d[1] == coord.y && d[2] == coord.z)
				{
					dir = i;
					
					break;
				}
			}
			
			return dir;
		}
		
		public static function Distance(coord1:CubeCoordinate,coord2:CubeCoordinate):Number
		{
			return (Math.abs(coord1.x - coord2.x) + Math.abs(coord1.y - coord2.y) + Math.abs(coord1.z - coord2.z)) / 2;
		}
		
		public function toString():String
		{
			return "[" + _x + "," + _y + "," + _z + "]";
		}
	}

}