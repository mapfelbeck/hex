package HexGrid
{
	
	/**
	 * ...
	 * @author Michael
	 */
	public interface IHexTileFactory
	{
		function CreateHexTile(coordinate:CubeCoordinate):IHexTile;
	}
	
}