package HexGrid
{
	/**
	 * ...
	 * @author Michael
	 */
	public class HexDirection
	{
		public static const UP:uint = 0;
		public static const UPPER_RIGHT:uint = 1;
		public static const LOWER_RIGHT:uint = 2;
		public static const DOWN:uint = 3;
		public static const LOWER_LEFT:uint = 4;
		public static const UPPER_LEFT:uint = 5;
		
		public static const directions:Array = [UP, UPPER_RIGHT, LOWER_RIGHT, DOWN, LOWER_LEFT, UPPER_LEFT];
		
		public static function OppositeDirection(dir:uint):uint
		{
			return HexDirection.directions[(dir + 3) % HexDirection.directions.length];
		}
		
		public static function DirectionName(directionVal:uint):String
		{
			var directionName:String = "unknown";
			
			switch(directionVal)
			{
				case UP:
					directionName = "up";
					break;
				case UPPER_RIGHT:
					directionName = "upper right";
					break;
				case LOWER_RIGHT:
					directionName = "lower right";
					break;
				case DOWN:
					directionName = "down";
					break;
				case LOWER_LEFT:
					directionName = "lower left";
					break;
				case UPPER_LEFT:
					directionName = "upper left";
					break;
			}
			
			return directionName;
		}
	}

}