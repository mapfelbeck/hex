package HexGrid
{
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import HexGrid.Events.*;
	/**
	 * ...
	 * @author Michael
	 */
	public class HexGrid extends EventDispatcher
	{
		private var factory:IHexTileFactory;
		
		private var gridSize:int = -1;
		public function get Size():int
		{
			return gridSize;
		}
		private var grid:Array;
		
		private var tiles:Vector.<IHexTile>;
		public function get Tiles():Vector.<IHexTile>
		{
			return tiles;
		}
		public var DeletedTiles:Vector.<IHexTile>;
		public var MovedTiles:Vector.<IHexTile>;
		public var ReplacementTiles:Vector.<ReplacementHex>;
		
		public function GridLookup(coord:AxialCoordinate):IHexTile
		{
			var returnedTile:IHexTile = null;
			if (null != grid[coord.q] && null != grid[coord.q][coord.r])
			{
				returnedTile = grid[coord.q][coord.r];
			}
			return returnedTile;
		}
		
		
		public function HexGrid(tileFactory:IHexTileFactory)
		{
			factory = tileFactory;
			grid = new Array();
			tiles = new Vector.<IHexTile>();
			DeletedTiles = new Vector.<IHexTile>();
			MovedTiles = new Vector.<IHexTile>();
			ReplacementTiles = new Vector.<ReplacementHex>();
		}
		
		public function GenerateTilesFromCoordSpace(coordinateSpace:Vector.<CubeCoordinate>):Boolean
		{
			var success:Boolean = true;
			
			if (tiles.length != 0)
			{
				throw new Error("GenerateTilesFromCoordSpace with an empty HexGrid object");
				success = false;
			}
			else
			{
				for each(var coord:CubeCoordinate in coordinateSpace)
				{
					//trace(coord.toString());
					//var sprite:TileSprite = new TileSprite();
					var tile:IHexTile = factory.CreateHexTile(coord); //new HexTile(sprite, coord);
					
					var tileAdded:Boolean = AddTileToGrid(tile);
					if (!tileAdded)
					{
						success = false;
						break;
					}
				}
			}
			
			return success;
		}
		
		public function AddTileToGrid(tile:IHexTile):Boolean
		{
			var success:Boolean = true;
			
			var axialCoord:AxialCoordinate = tile.axialCoordinate;
			
			if (null == grid[axialCoord.q])
			{
				//trace("q of " + axialCoord.q + " is null");
				grid[axialCoord.q] = new Array();
			}
			
			if (null != grid[axialCoord.q][axialCoord.r])
			{
				throw new Error("HexGrid cannot add tile in location " + axialCoord.toString() + " because one already exists there");
				success = false;
			}
			else
			{
				grid[axialCoord.q][axialCoord.r] = tile;
			}
			tiles.push(tile);
			
			return success;
		}
		
		private var lastSwapDirection:uint = HexDirection.DOWN;
		public function SwapTiles(first:IHexTile, second:IHexTile):Boolean
		{
			//trace("Grid: SwapTiles");
			var success:Boolean = true;
			
			if (null == first || null == second)
			{
				trace("Grid: SwapTiles passed at least 1 null var");
				success = false;
			}
			else if(first == second)
			{
				trace("Grid: SwapTiles cannot swap a tile with itself");
				success = false;
			}
			
			if (success)
			{
				var i:uint = 0;
				//direction as coordinate = 2nd one's coords minus 1st
				var dQ:int = second.axialCoordinate.q - first.axialCoordinate.q;
				var dR:int = second.axialCoordinate.r - first.axialCoordinate.r;
				
				lastSwapDirection = AxialCoordinate.AdjacentCoordToDirection(new AxialCoordinate(dQ,dR));
				
				//trace("Grid: tiles swapped in direction: " + lastSwapDirection);
				
				//swap coords
				var tempCoord:AxialCoordinate = first.axialCoordinate;
				first.axialCoordinate = second.axialCoordinate;
				second.axialCoordinate = tempCoord;
				
				//swap grid locations
				grid[first.axialCoordinate.q][first.axialCoordinate.r] = first;
				grid[second.axialCoordinate.q][second.axialCoordinate.r] = second;
				
				//mark as moved
				MovedTiles.push(first);
				MovedTiles.push(second);
				
				dispatchEvent(new GridContentsChangedEvent(HexEvents.GRID_CONTENTS_CHANGED));
			}
			
			return success;
		}
		
		public function Delete(toDelete:IHexTile):Boolean
		{
			var deleted:Boolean = true;
			
			if ( -1 != DeletedTiles.indexOf(toDelete))
			{
				//trace("tile already on delete list, doing nothing");
				deleted = false;
			}
			else
			{
				DeletedTiles.push(toDelete);
				var deleteCoords:AxialCoordinate = toDelete.axialCoordinate;
				grid[deleteCoords.q][deleteCoords.r] = null;
				var index:int = tiles.indexOf(toDelete);
				if (index >= 0)
				{
					tiles.splice(index, 1);
				}
				index = tiles.indexOf(toDelete);
				
				var deletedLocations:Vector.<AxialCoordinate> = new Vector.<AxialCoordinate>();
				deletedLocations.push(deleteCoords);
				
				toDelete.Destroy();
				
				CompressGrid(deletedLocations);
				
				dispatchEvent(new GridContentsChangedEvent(HexEvents.GRID_CONTENTS_CHANGED));
			}
			
			return deleted;
		}
		
		//private static var COMPRESS_DIRECTION:uint = HexDirection.DOWN;
		private function CompressGrid(deletedCoords:Vector.<AxialCoordinate>):void
		{
			//trace("CompressGrid");
			
			//this isn't really good general case because it assumes the board is symetric
			//and based around a zero-coordinate, using GridLookup(...) may be better
			while (deletedCoords.length > 0)
			{
				//trace("I see " + deletedTiles.length + " deleted tiles I have to deal with");
				
				var zeroZeroTile:AxialCoordinate = new AxialCoordinate(0, 0);
				var lookupDirection:uint = HexDirection.OppositeDirection(lastSwapDirection);
				
				var startCoord:AxialCoordinate = deletedCoords.pop();
				//trace("tiles with coords " + startCoord.toString() + "(axial) deleted");
				
				var lookupCoord:AxialCoordinate = startCoord.GetCoordInDirection(lookupDirection);
				var lookupTile:IHexTile = null;
				var replacementFound:Boolean = false;
				while (!replacementFound && AxialCoordinate.Distance(zeroZeroTile, lookupCoord) <= gridSize)
				{
					//trace("looking in (axial): " + lookupCoord.toString());
					lookupTile = GridLookup(lookupCoord);
					if (null != lookupTile)
					{
						//trace("well fuck me we found one!");
						replacementFound = true;
					}
					else
					{
						//done, at an empty spot on the board
						//trace("no tile for you!");
						break;
					}
				}
				if (replacementFound)
				{
					//don't add a tile to the move list more than once
					if ( -1 == MovedTiles.indexOf(lookupTile))
					{
						MovedTiles.push(lookupTile);
					}
					
					//trace("moving found tile from " + lookupCoord.toString() + " to " + startCoord.toString());
					lookupTile.axialCoordinate = startCoord;
					grid[lookupCoord.q][lookupCoord.r] = null;
					grid[startCoord.q][startCoord.r] = lookupTile;
					deletedCoords.push(lookupCoord);
				}
				else
				{
					//trace("I think " + lookupCoord.toString() + " needs to be replaced");
					var newTile:IHexTile = factory.CreateHexTile(lookupCoord.GetCoordInDirection(lastSwapDirection).AsCube());
					var replaceHex:ReplacementHex = new ReplacementHex(newTile, lookupDirection);
					ReplacementTiles.push(replaceHex);
				}
			}
		}
		
		/*
		as psuedo code:
		for each -N ≤ Δx ≤ N:
			for each max(-N, -Δx-N) ≤ Δy ≤ min(N, -Δx+N):
				Δz = -Δx-Δy
				results.append(H.add(Cube(Δx, Δy, Δz)))
		 */
		public function GenerateCoordinateSpace(range:int):Vector.<CubeCoordinate>
		{
			gridSize = range;
			var space:Vector.<CubeCoordinate> = new Vector.<CubeCoordinate>();
			
			//space.push(new CubeCoordinate(0, 0, 0));
			var x:int = 0;
			var y:int = 0;
			var z:int = 0;
			
			var minY:int;
			var maxY:int;
			
			for (x = -range; x <= range; x++)
			{
				minY = Math.max( -range, -x - range);
				maxY = Math.min( range, -x + range);
				for (y = minY; y <= maxY; y++)
				{
					z = -x - y;
					//trace("[" + x + "," + y + "," + z + "]");
					space.push(new CubeCoordinate(x, y, z));
				}
			}
			
			return space;
		}
	}

}