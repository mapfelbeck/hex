package
{
	import flash.system.Capabilities;
	
	/**
	 * ...
	 * @author Michael
	 */
	public class AirConfig implements IConfig
	{
		public function get ScreenWidth():int
		{
			return Capabilities.screenResolutionX;
		}
		
		public function get ScreenHeight():int
		{
			return Capabilities.screenResolutionY;
		}
		
		public function AirConfig()
		{
			
		}
		
		
		public function get BoardSize():int
		{
			return 3;
		}
	}

}