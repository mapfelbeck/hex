package
{
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.engine.GraphicElement;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Michael
	 */
	public class TileSprite extends MovieClip
	{
		public var xVar:TextField;
		public var yVar:TextField;
		public var zVar:TextField;
		
		public var qVar:TextField;
		public var rVar:TextField;
		
		public var center:Sprite;
		public var border:Sprite;
		
		private var gem:BitmapData;
		public var image:Sprite;
		
		public function TileSprite()
		{
			if (null == gem)
			{
				var rand = Math.random();
				//trace(rand);
				if (rand <= 0.1)
				{
					gem = new CutGem();
				}
				else
				{
					gem = new Gem();
				}
			}
			
			var gemBitmap:Bitmap = new Bitmap(gem);
			image.addChild(gemBitmap);
		}
	}

}