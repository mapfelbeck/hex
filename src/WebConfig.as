package
{
	import flash.display.Stage;
	/**
	 * ...
	 * @author Michael
	 */
	public class WebConfig implements IConfig
	{
		public function get ScreenWidth():int
		{
			return stageWidth;
		}
		private var stageWidth:int;
		
		public function get ScreenHeight():int
		{
			return stageHeight;
		}
		private var stageHeight:int;
		
		public function WebConfig(stage:Stage)
		{
			stageWidth = stage.stageWidth;
			stageHeight = stage.stageHeight;
		}
		
		
		public function get BoardSize():int
		{
			return 3;
		}
	}

}