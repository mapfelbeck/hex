﻿package  
{
	import flash.display.*;
	import flash.events.Event;
	import Screens.*;
	/**
	 * ...
	 * @author ...
	 */
	public class ScreenManager extends MovieClip 
	{
		var screens:Vector.<BaseScreen>;
		public function ScreenManager() 
		{
			trace("ScreenManager created");
			
			screens = new Vector.<BaseScreen>();
			
			//addEventListener(Event.ADDED, Added);
			//addEventListener(Event.ACTIVATE, Activate);
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Activate(event:Event)
		{
			trace("Activate");
		}
		private function Added(event:Event)
		{
			trace("Added");
		}
		
		private function Init(event:Event)
		{
			trace("Init");
			
			var menuScreen:BaseScreen = new MenuScreen();
			menuScreen.addEventListener(Event.ADDED_TO_STAGE, TurnOnMenu);
			this.stage.addChild(menuScreen);
		}
		
		private function TurnOnMenu(event:Event)
		{
			trace("TurnOnMenu");
			
			event.currentTarget.removeEventListener(Event.ADDED_TO_STAGE, TurnOnMenu);
			
			//event.currentTarget.visible = true;
		}
	}

}