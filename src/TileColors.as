package
{
	import util.Random;
	/**
	 * ...
	 * @author Michael
	 */
	public class TileColors
	{
		public static const RED:uint = 0xFF2222;
		public static const GREEN:uint = 0x22FF22;
		public static const BLUE:uint = 0x2222FF;
		public static const PURPLE:uint = 0xFF22FF;
		public static const CYAN:uint = 0x22FFFF;
		public static const YELLOW:uint = 0xFFFF22;
		public static const WHITE:uint = 0xDDDDDD;
		
		public static const colors:Array = new Array( RED, GREEN, BLUE, PURPLE, CYAN, YELLOW, WHITE );
		//public static const colors:Array = new Array(RED, GREEN);
		
		public function TileColors()
		{
		}
		
		public static function ColorName(colorVal:uint):String
		{
			var colorName:String = "unknown";
			
			switch(colorVal)
			{
				case RED:
					colorName = "red";
					break;
				case GREEN:
					colorName = "green";
					break;
				case BLUE:
					colorName = "blue";
					break;
				case PURPLE:
					colorName = "purple";
					break;
				case CYAN:
					colorName = "cyan";
					break;
				case YELLOW:
					colorName = "yellow";
					break;
				case WHITE:
					colorName = "white";
					break;
			}
			
			return colorName;
		}
		private static var count:int = 0;
		public static function RandomColor():uint
		{
			return colors[Random.randomNumber(0, colors.length - 1)];
		}
	}

}