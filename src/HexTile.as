package
{
	//see this for hex grid infodump:
	//http://www.redblobgames.com/grids/hexagons/
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.ColorTransform;
	import flash.filters.GlowFilter;
	import flash.filters.ColorMatrixFilter;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import HexGrid.Events.*;
	import HexGrid.*;
	/**
	 * ...
	 * @author Michael
	 */
	public class HexTile extends EventDispatcher implements IHexTile
	{
		//private var glow:GlowFilter;
		
		private var color:uint;
		public function get Color():uint
		{
			return color;
		}
		
		private var sprite:TileSprite;
		public function get HexSprite():MovieClip
		{
			return sprite;
		}
		public function set HexSprite(newSprite:MovieClip):void
		{
			sprite = newSprite as TileSprite;
		}
		
		private var cubeCoord:CubeCoordinate;
		public function get cubeCoordinate():CubeCoordinate
		{
			return cubeCoord;
		}
		public function set cubeCoordinate(coord:CubeCoordinate):void
		{
			cubeCoord = coord;
			axialCoord = cubeCoord.AsAxial();
			
			sprite.xVar.text = cubeCoord.x.toString();
			sprite.yVar.text = cubeCoord.y.toString();
			sprite.zVar.text = cubeCoord.z.toString();
			
			sprite.qVar.text = axialCoord.q.toString();
			sprite.rVar.text = axialCoord.r.toString();
		}
		
		private var axialCoord:AxialCoordinate;
		public function get axialCoordinate():AxialCoordinate
		{
			return axialCoord;
		}
		public function set axialCoordinate(coord:AxialCoordinate):void
		{
			axialCoord = coord;
			cubeCoordinate = axialCoord.AsCube();
		}
		
		public function HexTile(graphic:TileSprite, coordinate:CubeCoordinate, tileColor:uint)
		{
			//trace("HexTile constructor");
			HexSprite = graphic;
			cubeCoordinate = coordinate;
			
			color = tileColor;
			
			/*var myColorTransform:ColorTransform = new ColorTransform();
			myColorTransform.color = color;
			sprite.image.transform.colorTransform = myColorTransform;*/
			
			var redComponent:uint = (tileColor & 0xff0000) >> 16;
			var redRatio:Number = redComponent / 255;
			var greenComponent:uint = (tileColor & 0x00ff00) >> 8;
			var greenRatio:Number = greenComponent / 255;
			var blueComponent:uint = (tileColor & 0x0000ff) >> 0;
			var blueRatio:Number = blueComponent / 255;
			
			var matrix:Array = new Array();
			matrix = matrix.concat([redRatio, 0, 0, 0, 0]); // red
			matrix = matrix.concat([0, greenRatio, 0, 0, 0]); // green
			matrix = matrix.concat([0, 0, blueRatio, 0, 0]); // blue
			matrix = matrix.concat([0, 0, 0, 1, 0]); // alpha
			
			applyFilter(sprite.image, matrix);
			
			var showCoords:Boolean = false;
			sprite.xVar.visible = showCoords;
			sprite.yVar.visible = showCoords;
			sprite.zVar.visible = showCoords;
			
			sprite.qVar.visible = showCoords;
			sprite.rVar.visible = showCoords;
		}
		
		private function applyFilter(child:DisplayObject, matrix:Array):void {
			var filter:ColorMatrixFilter = new ColorMatrixFilter(matrix);
			var filters:Array = new Array();
			filters.push(filter);
			child.filters = filters;
		}
		
		public function Destroy():void
		{
			RemoveEventListeners();
		}
		
		public function ConnectEventListeners():void
		{
			if (Multitouch.supportsTouchEvents)
			{
				//trace("Touch events supported");
				connectTouchEvents();
			}
			else
			{
				connectMouseEvents();
			}
		}
		
		private function connectTouchEvents():void
		{
			HexSprite.addEventListener(TouchEvent.TOUCH_BEGIN, TileBegin);
			HexSprite.addEventListener(TouchEvent.TOUCH_END, TileEnd);
			HexSprite.addEventListener(TouchEvent.TOUCH_OVER, TileOver);
			HexSprite.addEventListener(TouchEvent.TOUCH_OUT, TileOut);
		}
		
		private function connectMouseEvents():void
		{
			HexSprite.addEventListener(MouseEvent.ROLL_OVER, MouseOver);
			HexSprite.addEventListener(MouseEvent.ROLL_OUT, MouseOut);
			HexSprite.addEventListener(MouseEvent.MOUSE_UP, MouseUp);
			HexSprite.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown);
		}
		
		public function RemoveEventListeners():void
		{
			if (Multitouch.supportsTouchEvents)
			{
				//trace("Touch events supported: " + Multitouch.supportsTouchEvents);
				removeTouchEvents();
			}
			else
			{
				removeMouseEvents();
			}
		}
		
		private function removeTouchEvents():void
		{
			HexSprite.removeEventListener(TouchEvent.TOUCH_OVER, TileOver);
			HexSprite.removeEventListener(TouchEvent.TOUCH_OUT, TileOut);
			HexSprite.removeEventListener(TouchEvent.TOUCH_BEGIN, TileBegin);
			HexSprite.removeEventListener(TouchEvent.TOUCH_END, TileEnd);
		}
		
		private function removeMouseEvents():void
		{
			HexSprite.removeEventListener(MouseEvent.ROLL_OVER, MouseOver);
			HexSprite.removeEventListener(MouseEvent.ROLL_OUT, MouseOut);
			HexSprite.removeEventListener(MouseEvent.MOUSE_DOWN, MouseUp);
			HexSprite.removeEventListener(MouseEvent.MOUSE_UP, MouseDown);
		}
		
		public function MouseOver(event:MouseEvent):void
		{
			//trace("mouse over");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_OVER);
			this.dispatchEvent(clickEvent);
		}
		
		public function MouseOut(event:MouseEvent):void
		{
			//trace("mouse out");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_OUT);
			this.dispatchEvent(clickEvent);
		}
		
		public function MouseDown(event:MouseEvent):void
		{
			//trace("mouse down");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_BEGIN);
			this.dispatchEvent(clickEvent);
		}
		
		public function MouseUp(event:MouseEvent):void
		{
			//trace("mouse down");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_END);
			this.dispatchEvent(clickEvent);
		}
		
		public function TileOver(event:TouchEvent):void
		{
			//trace("TileOver");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_OVER);
			this.dispatchEvent(clickEvent);
		}
		
		public function TileOut(event:TouchEvent):void
		{
			//trace("TileOut");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_OUT);
			this.dispatchEvent(clickEvent);
		}
		
		public function TileBegin(event:TouchEvent):void
		{
			//trace("TileOver");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_BEGIN);
			this.dispatchEvent(clickEvent);
		}
		
		public function TileEnd(event:TouchEvent):void
		{
			//trace("TileOver");
			var clickEvent:TileClickEvent = new TileClickEvent(HexEvents.TILE_END);
			this.dispatchEvent(clickEvent);
		}
	}
}