package
{
	import flash.errors.IllegalOperationError;
	/**
	 * ...
	 * @author Michael
	 */
	public class Callback
	{
		//callbacks get used once, if you need >1 it's an event so use
		//EventListener and EventDispatcher
		private var disposed:Boolean = false;
		
		private var method:Function = null;
		//context, sometimes matters with funky factory patterns
		//does NOT work like AS2
		private var context:Object = null;
		private var args:Array = null;
		
		public function Callback(func:Function, ctx:Object, ...rest)
		{
			//trace("Callback.Callback()");
			if (null == func)
			{
				throw new ArgumentError("Callback constructor requires non-null function arg");
			}
			method = func;
			context = ctx;
			args = rest;
		}
		
		public function Exec(...rest):void
		{
			//trace("Callback.Exec()");
			if (disposed)
			{
				throw new IllegalOperationError("Cannot Exec() a disposed Callback");
			}
			if (rest.length > 0)
			{
				args = args.concat(rest);
			}
			method.apply(context, args);
			Dispose();
		}
		
		//dangling references are bad, mkay?
		public function Dispose():void
		{
			//trace("Callback.Dispose()");
			disposed = true;
			
			method = null;
			context = null;
			args = null;
		}
	}
}