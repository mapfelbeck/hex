package
{
	/**
	 * ...
	 * @author Michael
	 */
	public interface IConfig
	{
		function get ScreenWidth():int;
		function get ScreenHeight():int;
		function get BoardSize():int;
	}

}